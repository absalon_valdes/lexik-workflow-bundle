<?php

namespace Lexik\Bundle\WorkflowBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Lexik\Bundle\WorkflowBundle\Flow\NextStateInterface;

/**
 * This is the class that validates and merges configuration from your imported process definition files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class ProcessConfiguration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('definition');

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('start')
                    ->defaultNull()
                ->end()
                ->arrayNode('end')
                    ->defaultValue(array())
                    ->prototype('scalar')->end()
                ->end()
            ->end()
            ->append($this->createStepsNodeDefinition())
        ;

        return $treeBuilder;
    }

    /**
     * Create a configuration node to define the steps of a process.
     *
     * @return ArrayNodeDefinition
     */
    private function createStepsNodeDefinition()
    {
        $stepsNode = new ArrayNodeDefinition('steps');

        $stepsNode
            ->defaultValue(array())
            ->useAttributeAsKey('name')
            ->prototype('array')
                ->addDefaultsIfNotSet()
                ->children()
                    ->scalarNode('label')
                        ->defaultValue('')
                    ->end()
                    ->arrayNode('roles')
                        ->prototype('scalar')->end()
                    ->end()
                    ->arrayNode('model_status')
                        ->validate()
                            ->ifTrue(function ($value) {
                                return (is_array($value) && count($value) < 2);
                            })
                            ->thenInvalid('You must specify an array with [ method, constant ]')
                            ->ifTrue(function ($value) {
                                return ( ! defined($value[1]));
                            })
                            ->thenInvalid('You must specify a valid constant name as second parameter')
                        ->end()
                        ->prototype('scalar')->end()
                    ->end()
                    ->scalarNode('on_invalid')
                        ->defaultNull()
                    ->end()
                ->end()
                ->append($this->createNextStatesNodeDefinition())
            ->end()
        ;

        return $stepsNode;
    }

    /**
     * Create a configuration node to define available next states of a step (or a processs)
     *
     * @return ArrayNodeDefinition
     */
    private function createNextStatesNodeDefinition()
    {
        $flowTypes = array(
            NextStateInterface::TYPE_STEP,
            NextStateInterface::TYPE_STEP_OR,
            NextStateInterface::TYPE_PROCESS,
        );

        $nextStatesNode = new ArrayNodeDefinition('next_states');

        $nextStatesNode
            ->useAttributeAsKey('name')
            ->prototype('array')
                ->addDefaultsIfNotSet()
                ->children()
                    ->scalarNode('type')
                        ->defaultValue('step')
                        ->validate()
                             ->ifNotInArray($flowTypes)
                             ->thenInvalid('Invalid next element type "%s". Please use one of the following types: '.implode(', ', $flowTypes))
                        ->end()
                    ->end()
                    ->variableNode('target')
                        ->cannotBeEmpty()
                    ->end()
                ->end()
            ->end()
        ;

        return $nextStatesNode;
    }
}
